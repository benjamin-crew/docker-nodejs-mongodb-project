# docker-nodejs-mongodb-project

This is a project that takes a simple html frontend, nodejs backend, mongodb, and hosts it all in docker.

This project is a follow-along of TechWorld With Nana's Docker Tutorial:
https://www.youtube.com/watch?v=3c-iBn73dDE&t=1827s

https://gitlab.com/nanuchi/techworld-js-docker-demo-app